
# herbert

> cli system monitor... just to learn some stuff

## Requirements

- /proc
- lscpu,lsblk
- lsb_release -a

## Example cli application
```
herbert             # outputs all JSON and exit
herbert ui          # show dashboard UI
herbert cli         # start interactive shell
herbert serve       # start HTTP JSON API
```
### Options
```
herbert --format=(json|table|etc) [default:json]
herbert --module=(info,cpu,memory,disk,network) [default:all]

herbert ui --frequency=1sec --period=(N * requency)
    eg. --frequency=2sec --period=10

herbert serve --addr=0.0.0.0 --port=55551 --token=Test1234
```
## Modules

### Info Module
```
/proc/uptime
/proc/version
/proc/loadavg
/proc/sys/kernel/hostname
/proc/sys/kernel/ostype
/proc/sys/kernel/osrelease
/proc/sys/kernel/version
```
```
lsb_release -a
```
### CPU Module
```
/proc/cpuinfo
```
```
$ lscpu -e=CPU,SOCKET,CORE,ONLINE,MAXMHZ --json
{
   "cpus": [
      {"cpu": "0", "socket": "0", "core": "0", "online": "yes", "maxmhz": "3100.0000"},
      {"cpu": "1", "socket": "0", "core": "1", "online": "yes", "maxmhz": "3100.0000"},
      {"cpu": "2", "socket": "0", "core": "0", "online": "yes", "maxmhz": "3100.0000"},
      {"cpu": "3", "socket": "0", "core": "1", "online": "yes", "maxmhz": "3100.0000"}
   ]
}
```
```
lscpu --json
{
   "lscpu": [
      {"field": "Architecture:", "data": "x86_64"},
      {"field": "CPU op-mode(s):", "data": "32-bit, 64-bit"},
      {"field": "Byte Order:", "data": "Little Endian"},
      {"field": "CPU(s):", "data": "4"},
      {"field": "On-line CPU(s) list:", "data": "0-3"},
      {"field": "Thread(s) per core:", "data": "2"},
      {"field": "Core(s) per socket:", "data": "2"},
      {"field": "Socket(s):", "data": "1"},
      {"field": "NUMA node(s):", "data": "1"},
      {"field": "Vendor ID:", "data": "GenuineIntel"},
      {"field": "CPU family:", "data": "6"},
      {"field": "Model:", "data": "69"},
      {"field": "Model name:", "data": "Intel(R) Core(TM) i7-4510U CPU @ 2.00GHz"},
      {"field": "Stepping:", "data": "1"},
      {"field": "CPU MHz:", "data": "798.187"},
      {"field": "CPU max MHz:", "data": "3100.0000"},
      {"field": "CPU min MHz:", "data": "800.0000"},
      {"field": "BogoMIPS:", "data": "5187.95"},
      {"field": "Virtualisation:", "data": "VT-x"},
      {"field": "L1d cache:", "data": "32K"},
      {"field": "L1i cache:", "data": "32K"},
      {"field": "L2 cache:", "data": "256K"},
      {"field": "L3 cache:", "data": "4096K"},
      {"field": "NUMA node0 CPU(s):", "data": "0-3"},
      {"field": "Flags:", "data": "fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm constant_tsc arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid aperfmperf pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 sdbg fma cx16 xtpr pdcm pcid sse4_1 sse4_2 movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm abm cpuid_fault epb invpcid_single pti ssbd ibrs ibpb stibp tpr_shadow vnmi flexpriority ept vpid fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid xsaveopt dtherm ida arat pln pts flush_l1d"}
   ]
}
```
### Memory Module
```
/proc/meminfo
/proc/swaps
```
### Network Module
```
//Interface speeds
// R1=`cat /sys/class/net/$1/statistics/rx_bytes`
// T1=`cat /sys/class/net/$1/statistics/tx_bytes`
// sleep $INTERVAL
// R2=`cat /sys/class/net/$1/statistics/rx_bytes`
// T2=`cat /sys/class/net/$1/statistics/tx_bytes`
// TBPS=`expr $T2 - $T1`
// RBPS=`expr $R2 - $R1`
// TKBPS=`expr $TBPS / 1024`
// RKBPS=`expr $RBPS / 1024`
// echo "TX $1: $TKBPS kB/s RX $1: $RKBPS kB/s"

/proc/net/route
/proc/net/netstat

/proc/sys/net/netfilter/nf_conntrack_acct
/proc/sys/net/netfilter/nf_conntrack_buckets
/proc/sys/net/netfilter/nf_conntrack_checksum
/proc/sys/net/netfilter/nf_conntrack_count
/proc/sys/net/netfilter/nf_conntrack_dccp_loose
/proc/sys/net/netfilter/nf_conntrack_dccp_timeout_closereq
/proc/sys/net/netfilter/nf_conntrack_dccp_timeout_closing
/proc/sys/net/netfilter/nf_conntrack_dccp_timeout_open
/proc/sys/net/netfilter/nf_conntrack_dccp_timeout_partopen
/proc/sys/net/netfilter/nf_conntrack_dccp_timeout_request
/proc/sys/net/netfilter/nf_conntrack_dccp_timeout_respond
/proc/sys/net/netfilter/nf_conntrack_dccp_timeout_timewait
/proc/sys/net/netfilter/nf_conntrack_events
/proc/sys/net/netfilter/nf_conntrack_expect_max
/proc/sys/net/netfilter/nf_conntrack_frag6_high_thresh
/proc/sys/net/netfilter/nf_conntrack_frag6_low_thresh
/proc/sys/net/netfilter/nf_conntrack_frag6_timeout
/proc/sys/net/netfilter/nf_conntrack_generic_timeout
/proc/sys/net/netfilter/nf_conntrack_helper
/proc/sys/net/netfilter/nf_conntrack_icmp_timeout
/proc/sys/net/netfilter/nf_conntrack_icmpv6_timeout
/proc/sys/net/netfilter/nf_conntrack_log_invalid
/proc/sys/net/netfilter/nf_conntrack_max
/proc/sys/net/netfilter/nf_conntrack_sctp_timeout_closed
/proc/sys/net/netfilter/nf_conntrack_sctp_timeout_cookie_echoed
/proc/sys/net/netfilter/nf_conntrack_sctp_timeout_cookie_wait
/proc/sys/net/netfilter/nf_conntrack_sctp_timeout_established
/proc/sys/net/netfilter/nf_conntrack_sctp_timeout_heartbeat_acked
/proc/sys/net/netfilter/nf_conntrack_sctp_timeout_heartbeat_sent
/proc/sys/net/netfilter/nf_conntrack_sctp_timeout_shutdown_ack_sent
/proc/sys/net/netfilter/nf_conntrack_sctp_timeout_shutdown_recd
/proc/sys/net/netfilter/nf_conntrack_sctp_timeout_shutdown_sent
/proc/sys/net/netfilter/nf_conntrack_tcp_be_liberal
/proc/sys/net/netfilter/nf_conntrack_tcp_loose
/proc/sys/net/netfilter/nf_conntrack_tcp_max_retrans
/proc/sys/net/netfilter/nf_conntrack_tcp_timeout_close
/proc/sys/net/netfilter/nf_conntrack_tcp_timeout_close_wait
/proc/sys/net/netfilter/nf_conntrack_tcp_timeout_established
/proc/sys/net/netfilter/nf_conntrack_tcp_timeout_fin_wait
/proc/sys/net/netfilter/nf_conntrack_tcp_timeout_last_ack
/proc/sys/net/netfilter/nf_conntrack_tcp_timeout_max_retrans
/proc/sys/net/netfilter/nf_conntrack_tcp_timeout_syn_recv
/proc/sys/net/netfilter/nf_conntrack_tcp_timeout_syn_sent
/proc/sys/net/netfilter/nf_conntrack_tcp_timeout_time_wait
/proc/sys/net/netfilter/nf_conntrack_tcp_timeout_unacknowledged
/proc/sys/net/netfilter/nf_conntrack_timestamp
/proc/sys/net/netfilter/nf_conntrack_udp_timeout
/proc/sys/net/netfilter/nf_conntrack_udp_timeout_stream
/proc/sys/net/nf_conntrack_max

```
### Disk Module
```
sudo hdparm -I /dev/sda
sudo lshw -class disk -class storage
sudo lshw -short -C disk
df -x=squashfs,tmpfs \
	--output=source,fstype,itotal,iused,iavail,ipcent,size,used,avail,pcent,file,target
df -x devtmpfs -x tmpfs -x squashfs --output=source,fstype,itotal,iused,iavail,ipcent,size,used,avail,pcent,file,target

cat /proc/mounts
cat /proc/scsi/scsi
cat /proc/partitions
cat /proc/diskstats

```

### EC2 Instance Module
```
ec2metadata
ami-id: ami-fcc19b99
ami-launch-index: 0
ami-manifest-path: (unknown)
ancestor-ami-ids: unavailable
availability-zone: us-east-2b
block-device-mapping: ami
root
instance-action: none
instance-id: i-07baa11ddadefd0d1
instance-type: t2.medium
local-hostname: ip-10-0-1-173.us-east-2.compute.internal
local-ipv4: 10.0.1.173
kernel-id: unavailable
mac: unavailable
profile: default-hvm
product-codes: unavailable
public-hostname: ec2-52-14-37-187.us-east-2.compute.amazonaws.com
public-ipv4: 52.14.37.187
public-keys: ['ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCKHn6mrTnbOdotT6vCC/X2RrDaUD8j6QS5oTA7Wtohiv+kfSqROO/xs6CLgXlVANQ4RLsFLkkpsyrGxlP2Lxk6eK1MqxGnDjGx1VCYF9cPE15o79Mwd+Q6rltPg3J9TTLzSCqnTx+E8oKj7fTvDXntmUeRnai3c0HqZEougTye7V/waX0vBYfYfYrKDwaSimFYvryIzPhgTR9Kt2yG/UVaSnn0M6cbP1eu28Y4S2LWorB3J2RfSsakEr2pHP6aOhF2UcyWdgwkvWG695zQLNmUpK7dbAboUoIuH289IE1qGRGRPv34MIfBcLbvhImMf2Fr9ufOTQq6H7mzefV6B469 Some_Key']
ramdisk-id: unavailable
reserveration-id: unavailable
security-groups: Scotia-Stage-Mgmt-SG
user-data: unavailable
```