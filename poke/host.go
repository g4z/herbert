package poke

import (
	"encoding/json"

	"./modules"
)

// Host defines the top-level host object
type Host struct {
	CPU     *modules.CPUMonitor
	Disk    *modules.DiskMonitor
	Info    *modules.InfoMonitor
	Memory  *modules.MemoryMonitor
	Network *modules.NetworkMonitor
}

// NewHost returns a new top-level host object
func NewHost() (*Host, error) {

	host := &Host{}

	cpu, err := modules.NewCPUMonitor()
	if err != nil {
		return nil, err
	}

	disk, err := modules.NewDiskMonitor()
	if err != nil {
		return nil, err
	}

	info, err := modules.NewInfoMonitor()
	if err != nil {
		return nil, err
	}

	memory, err := modules.NewMemoryMonitor()
	if err != nil {
		return nil, err
	}

	network, err := modules.NewNetworkMonitor()
	if err != nil {
		return nil, err
	}

	host.CPU = cpu
	host.Disk = disk
	host.Info = info
	host.Memory = memory
	host.Network = network

	return host, nil
}

// String returns a JSON representation
func (host *Host) String() (string, error) {
	json, err := json.Marshal(host)
	if err != nil {
		return "", err
	}
	return string(json), nil
}

// Refresh updates the Host struct data
func (host *Host) Refresh() error {

	var err error

	if err = host.CPU.Refresh(); err != nil {
		return err
	}
	if err = host.Disk.Refresh(); err != nil {
		return err
	}
	if err = host.Info.Refresh(); err != nil {
		return err
	}
	if err = host.Memory.Refresh(); err != nil {
		return err
	}
	if err = host.Network.Refresh(); err != nil {
		return err
	}

	return nil
}
