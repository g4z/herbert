// Package modules provides primitives for system metrics
package modules

import "encoding/json"

// DiskMonitor structure for storing the disk metrics
type DiskMonitor struct {
	Name string
}

// NewDiskMonitor returns a new DiskMonitor structure
func NewDiskMonitor() (*DiskMonitor, error) {
	monitor := &DiskMonitor{}
	err := monitor.Refresh()
	if err != nil {
		return nil, err
	}
	return monitor, nil
}

// String returns a JSON representation
func (monitor *DiskMonitor) String() (string, error) {
	json, err := json.Marshal(monitor)
	if err != nil {
		return "", err
	}
	return string(json), nil
}

// Refresh updates the Host struct data
func (monitor *DiskMonitor) Refresh() error {
	// monitor.RefreshMemInfo()
	return nil
}
