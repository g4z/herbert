// Package modules provides primitives for system metrics
package modules

import (
	"bufio"
	"encoding/json"
	"regexp"
	"strconv"
	"strings"
)

// MemoryMonitor structure for system memory data
type MemoryMonitor struct {
	MemTotal     []int
	MemFree      []int
	MemAvailable []int
	// Buffers           []int
	// Cached            []int
	// SwapCached        []int
	// Active            []int
	// Inactive          []int
	// ActiveAnon        []int
	// InactiveAnon      []int
	// ActiveFile        []int
	// InactiveFile      []int
	// Unevictable       []int
	// Mlocked           []int
	SwapTotal []int
	SwapFree  []int
	// Dirty             []int
	// Writeback         []int
	// AnonPages         []int
	// Mapped            []int
	// Shmem             []int
	// Slab              []int
	// SReclaimable      []int
	// SUnreclaim        []int
	// KernelStack       []int
	// PageTables        []int
	// NFSUnstable       []int
	// Bounce            []int
	// WritebackTmp      []int
	// CommitLimit       []int
	// CommittedAS       []int
	// VmallocTotal      []int
	// VmallocUsed       []int
	// VmallocChunk      []int
	// HardwareCorrupted []int
	// AnonHugePages     []int
	// ShmemHugePages    []int
	// ShmemPmdMapped    []int
	// CmaTotal          []int
	// CmaFree           []int
	// HugePagesTotal    []int
	// HugePagesFree     []int
	// HugePagesRsvd     []int
	// HugePagesSurp     []int
	// Hugepagesize      []int
	// DirectMap4k       []int
	// DirectMap2M       []int
	// DirectMap1G       []int
}

// NewMemoryMonitor returns a new MemoryMonitor structure
func NewMemoryMonitor() (*MemoryMonitor, error) {
	monitor := &MemoryMonitor{}
	err := monitor.Refresh()
	if err != nil {
		return nil, err
	}
	return monitor, nil
}

// String returns a JSON representation
func (monitor *MemoryMonitor) String() (string, error) {
	json, err := json.Marshal(monitor)
	if err != nil {
		return "", err
	}
	return string(json), nil
}

// Refresh updates the Memory struct data
func (monitor *MemoryMonitor) Refresh() error {
	monitor.RefreshMemInfo()
	return nil
}

// RefreshMemInfo reloads the ostype from /proc/sys/kernel/ostype
func (monitor *MemoryMonitor) RefreshMemInfo() error {

	buf, err := ReadFile("/proc/meminfo")
	if err != nil {
		return err
	}

	scanner := bufio.NewScanner(strings.NewReader(buf))

	// MemTotal
	scanner.Scan()
	regex := regexp.MustCompile("MemTotal:\\s+([0-9]+)\\skB")
	match := regex.FindStringSubmatch(scanner.Text())
	memTotal, err := strconv.Atoi(match[1])
	if err != nil {
		return err
	}
	monitor.MemTotal = append(
		monitor.MemTotal,
		int(memTotal),
	)

	// MemFree
	scanner.Scan()
	regex = regexp.MustCompile("MemFree:\\s+([0-9]+)\\skB")
	match = regex.FindStringSubmatch(scanner.Text())
	memFree, err := strconv.Atoi(match[1])
	if err != nil {
		return err
	}
	monitor.MemFree = append(
		monitor.MemFree,
		int(memFree),
	)

	// MemAvailable
	scanner.Scan()
	regex = regexp.MustCompile("MemAvailable:\\s+([0-9]+)\\skB")
	match = regex.FindStringSubmatch(scanner.Text())
	memAvailable, err := strconv.Atoi(match[1])
	if err != nil {
		return err
	}
	monitor.MemAvailable = append(
		monitor.MemAvailable,
		int(memAvailable),
	)

	// skip these lines
	for i := 0; i < 11; i++ {
		scanner.Scan()
	}

	// SwapTotal
	scanner.Scan()
	regex = regexp.MustCompile("SwapTotal:\\s+([0-9]+)\\skB")
	match = regex.FindStringSubmatch(scanner.Text())
	swapTotal, err := strconv.Atoi(match[1])
	if err != nil {
		return err
	}
	monitor.SwapTotal = append(
		monitor.SwapTotal,
		int(swapTotal),
	)

	// SwapFree
	scanner.Scan()
	regex = regexp.MustCompile("SwapFree:\\s+([0-9]+)\\skB")
	match = regex.FindStringSubmatch(scanner.Text())
	swapFree, err := strconv.Atoi(match[1])
	if err != nil {
		return err
	}
	monitor.SwapFree = append(
		monitor.SwapFree,
		int(swapFree),
	)

	// Done
	return nil
}

//---------------------------------------------------------------

// GetCurrentMemTotal returns current memTotal value
func (monitor *MemoryMonitor) GetCurrentMemTotal() int {
	return monitor.MemTotal[len(monitor.MemTotal)-1]
}

// GetAllMemTotal returns all memTotal values
func (monitor *MemoryMonitor) GetAllMemTotal() []int {
	return monitor.MemTotal
}

//---------------------------------------------------------------

// GetCurrentMemFree returns current memFree value
func (monitor *MemoryMonitor) GetCurrentMemFree() int {
	return monitor.MemFree[len(monitor.MemFree)-1]
}

// GetAllMemFree returns all memFree values
func (monitor *MemoryMonitor) GetAllMemFree() []int {
	return monitor.MemFree
}

//---------------------------------------------------------------

// GetCurrentMemAvailable returns current memAvailable value
func (monitor *MemoryMonitor) GetCurrentMemAvailable() int {
	return monitor.MemAvailable[len(monitor.MemAvailable)-1]
}

// GetAllMemAvailable returns all memAvailable values
func (monitor *MemoryMonitor) GetAllMemAvailable() []int {
	return monitor.MemAvailable
}

//---------------------------------------------------------------

// GetCurrentSwapTotal returns current swapTotal value
func (monitor *MemoryMonitor) GetCurrentSwapTotal() int {
	return monitor.SwapTotal[len(monitor.SwapTotal)-1]
}

// GetAllSwapTotal returns all swapTotal values
func (monitor *MemoryMonitor) GetAllSwapTotal() []int {
	return monitor.SwapTotal
}

//---------------------------------------------------------------

// GetCurrentSwapFree returns current swapFree value
func (monitor *MemoryMonitor) GetCurrentSwapFree() int {
	return monitor.SwapFree[len(monitor.SwapFree)-1]
}

// GetAllSwapFree returns all swapFree values
func (monitor *MemoryMonitor) GetAllSwapFree() []int {
	return monitor.SwapFree
}
