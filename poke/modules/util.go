// Package modules provides primitives for system metrics
package modules

import (
	"bytes"
	"io/ioutil"
	"os"
	"os/exec"
	"time"
)

// ReadFile opens, reads and returns content from filepath
func ReadFile(filepath string) (string, error) {

	f, err := os.Open(filepath)
	if err != nil {
		return "", err
	}
	defer f.Close()

	buf, err := ioutil.ReadAll(f)
	if err != nil {
		return "", err
	}

	return string(buf), nil
}

// ReadProcess executes a command and reads the stdout
func ReadProcess(command string, arguments ...string) (string, error) {
	cmd := exec.Command(command, arguments...)
	var out bytes.Buffer
	cmd.Stdout = &out
	if err := cmd.Run(); err != nil {
		return "", err
	}
	return out.String(), nil
}

// FormatDuration converts seconds to a friendly string
// eg. "2d/3h/35m/2s"
func FormatDuration(t time.Duration) string {
	// TODO implement function FormatDuration()
	// if seconds < 60 {
	// 	return "< 1 Minute"
	// }
	// if seconds < 3600 {
	// 	return fmt.Sprintf("%d Minutes", seconds/60)
	// }
	// if seconds < 86400 {
	// 	return fmt.Sprintf("%d Hours", seconds/60/60)
	// }
	// return fmt.Sprintf("%d Days", seconds/60/60/24)
	return "2d/3h/35m/2s"
}
