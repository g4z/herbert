// Package modules provides primitives for system metrics
package modules

import (
	"bufio"
	"encoding/json"
	"fmt"
	"strings"
)

// InfoMonitor structure for generic system data
type InfoMonitor struct {
	Hostname        string
	OSType          string
	OSRelease       string
	OSVersion       string
	LSBDistribution string
	LSBDescription  string
	LSBRelease      string
	LSBCodename     string
	Uptime          []float64
	Idletime        []float64
	LoadAvg1        []float64
	LoadAvg5        []float64
	LoadAvg10       []float64
}

// NewInfoMonitor returns a new InfoMonitor structure
func NewInfoMonitor() (*InfoMonitor, error) {
	monitor := &InfoMonitor{}
	err := monitor.Refresh()
	if err != nil {
		return nil, err
	}
	return monitor, nil
}

// String returns a JSON representation
func (monitor *InfoMonitor) String() (string, error) {
	json, err := json.Marshal(monitor)
	if err != nil {
		return "", err
	}
	return string(json), nil
}

// Refresh updates the InfoMonitor struct data
func (monitor *InfoMonitor) Refresh() error {
	var err error
	if err = monitor.RefreshLSBRelease(); err != nil {
		return err
	}
	if err = monitor.RefreshHostname(); err != nil {
		return err
	}
	if err = monitor.RefreshOSType(); err != nil {
		return err
	}
	if err = monitor.RefreshOSRelease(); err != nil {
		return err
	}
	if err = monitor.RefreshOSVersion(); err != nil {
		return err
	}
	if err = monitor.RefreshUptime(); err != nil {
		return err
	}
	if err = monitor.RefreshLoadAvg(); err != nil {
		return err
	}
	return nil
}

// RefreshLSBRelease reloads data from `lsb_release -a` output
func (monitor *InfoMonitor) RefreshLSBRelease() error {

	buf, err := ReadProcess("/usr/bin/lsb_release", "-a")
	if err != nil {
		return err
	}

	var distribution string
	var description string
	var release string
	var codename string

	reader := strings.NewReader(buf)
	scanner := bufio.NewScanner(reader)

	scanner.Scan()
	if _, err = fmt.Sscanf(scanner.Text(),
		"Distributor ID: %s", &distribution); err != nil {
		return err
	}

	scanner.Scan()
	if _, err = fmt.Sscanf(scanner.Text(),
		"Description: %s", &description); err != nil {
		return err
	}

	scanner.Scan()
	if _, err = fmt.Sscanf(scanner.Text(),
		"Release: %s", &release); err != nil {
		return err
	}

	scanner.Scan()
	if _, err = fmt.Sscanf(scanner.Text(),
		"Codename: %s", &codename); err != nil {
		return err
	}

	monitor.LSBDistribution = distribution
	monitor.LSBDescription = description
	monitor.LSBRelease = release
	monitor.LSBCodename = codename

	return nil
}

// GetLSBDistribution returns the current LSB distribution value
func (monitor *InfoMonitor) GetLSBDistribution() string {
	return monitor.LSBDistribution
}

// GetLSBDescription returns the current LSB description value
func (monitor *InfoMonitor) GetLSBDescription() string {
	return monitor.LSBDescription
}

// GetLSBRelease returns the current LSB release value
func (monitor *InfoMonitor) GetLSBRelease() string {
	return monitor.LSBRelease
}

// GetLSBCodename returns the current LSB codename value
func (monitor *InfoMonitor) GetLSBCodename() string {
	return monitor.LSBCodename
}

// RefreshHostname reloads the hostname from /proc/sys/kernel/hostname
func (monitor *InfoMonitor) RefreshHostname() error {
	buf, err := ReadFile("/proc/sys/kernel/hostname")
	if err != nil {
		return err
	}
	monitor.Hostname = strings.TrimSpace(buf)
	return nil
}

// GetHostname returns the current hostname value
func (monitor *InfoMonitor) GetHostname() string {
	return monitor.Hostname
}

// RefreshOSType reloads the ostype from /proc/sys/kernel/ostype
func (monitor *InfoMonitor) RefreshOSType() error {
	buf, err := ReadFile("/proc/sys/kernel/ostype")
	if err != nil {
		return err
	}
	monitor.OSType = strings.TrimSpace(buf)
	return nil
}

// GetOSType returns current ostype value
func (monitor *InfoMonitor) GetOSType() string {
	return monitor.OSType
}

// RefreshOSRelease reloads the osrelease from /proc/sys/kernel/osrelease
func (monitor *InfoMonitor) RefreshOSRelease() error {
	buf, err := ReadFile("/proc/sys/kernel/osrelease")
	if err != nil {
		return err
	}
	monitor.OSRelease = strings.TrimSpace(buf)
	return nil
}

// GetOSRelease returns the current osrelease value
func (monitor *InfoMonitor) GetOSRelease() string {
	return monitor.OSRelease
}

// RefreshOSVersion reloads the version from /proc/sys/kernel/version
func (monitor *InfoMonitor) RefreshOSVersion() error {
	buf, err := ReadFile("/proc/sys/kernel/version")
	if err != nil {
		return err
	}
	monitor.OSVersion = strings.TrimSpace(buf)
	return nil
}

// GetOSVersion returns current version value
func (monitor *InfoMonitor) GetOSVersion() string {
	return monitor.OSVersion
}

// RefreshUptime reloads the uptime and idletime from /proc/uptime
func (monitor *InfoMonitor) RefreshUptime() error {

	buf, err := ReadFile("/proc/uptime")
	if err != nil {
		return err
	}

	var floatUp, floatIdle float64

	_, err = fmt.Sscanf(buf, "%f %f", &floatUp, &floatIdle)
	if err != nil {
		return err
	}

	monitor.Uptime = append(monitor.Uptime, floatUp)
	monitor.Idletime = append(monitor.Idletime, floatIdle)

	return nil
}

// GetCurrentUptime return current uptime in seconds
func (monitor *InfoMonitor) GetCurrentUptime() float64 {
	return monitor.Uptime[len(monitor.Uptime)-1]
}

// GetAllUptime return historical uptime data
func (monitor *InfoMonitor) GetAllUptime() []float64 {
	return monitor.Uptime
}

// GetCurrentUptimeString returns the current uptime as a string
func (monitor *InfoMonitor) GetCurrentUptimeString() string {
	// TODO Implement GetUptimeString
	// monitor.Uptime = FormatDuration(upSecsInt)
	// monitor.Idletime = FormatDuration(idleSecsInt)
	// intUp = int(math.Round(upSecs))
	// intIdle = int(math.Round(idleSecs))
	return "TODO"
}

// GetCurrentIdletime returns current idletime in seconds
func (monitor *InfoMonitor) GetCurrentIdletime() float64 {
	return monitor.Idletime[len(monitor.Idletime)-1]
}

// GetAllIdletime returns historical idletime data
func (monitor *InfoMonitor) GetAllIdletime() []float64 {
	return monitor.Idletime
}

// GetCurrentIdletimeString returns the current idletime as a string
func (monitor *InfoMonitor) GetCurrentIdletimeString() string {
	// TODO Implement GetIdletimeString
	// monitor.Uptime = FormatDuration(upSecsInt)
	// monitor.Idletime = FormatDuration(idleSecsInt)
	// intUp = int(math.Round(upSecs))
	// intIdle = int(math.Round(idleSecs))
	return "TODO"
}

// RefreshLoadAvg reloads load_avg from /proc/load_avg
func (monitor *InfoMonitor) RefreshLoadAvg() error {

	buf, err := ReadFile("/proc/loadavg")
	if err != nil {
		return err
	}

	var one, five, ten float64

	_, err = fmt.Sscanf(buf, "%f %f %f", &one, &five, &ten)
	if err != nil {
		return err
	}

	monitor.LoadAvg1 = append(monitor.LoadAvg1, one)
	monitor.LoadAvg5 = append(monitor.LoadAvg5, five)
	monitor.LoadAvg10 = append(monitor.LoadAvg10, ten)

	return nil
}

// GetCurrentLoadAvg returns current load_avg
func (monitor *InfoMonitor) GetCurrentLoadAvg() []float64 {
	return []float64{
		monitor.LoadAvg1[len(monitor.LoadAvg1)-1],
		monitor.LoadAvg5[len(monitor.LoadAvg5)-1],
		monitor.LoadAvg10[len(monitor.LoadAvg10)-1],
	}
}

// GetCurrentLoadAvg1 returns current load_avg
func (monitor *InfoMonitor) GetCurrentLoadAvg1() float64 {
	return monitor.LoadAvg1[len(monitor.LoadAvg1)-1]
}

// GetCurrentLoadAvg5 returns current load_avg
func (monitor *InfoMonitor) GetCurrentLoadAvg5() float64 {
	return monitor.LoadAvg5[len(monitor.LoadAvg5)-1]
}

// GetCurrentLoadAvg10 returns current load_avg
func (monitor *InfoMonitor) GetCurrentLoadAvg10() float64 {
	return monitor.LoadAvg10[len(monitor.LoadAvg10)-1]
}

// GetAllLoadAvg returns historical load_avg
func (monitor *InfoMonitor) GetAllLoadAvg() [][]float64 {
	return [][]float64{
		monitor.LoadAvg1,
		monitor.LoadAvg5,
		monitor.LoadAvg10,
	}
}

// GetAllLoadAvg1 returns historical load_avg
func (monitor *InfoMonitor) GetAllLoadAvg1() []float64 {
	return monitor.LoadAvg1
}

// GetAllLoadAvg5 returns historical load_avg
func (monitor *InfoMonitor) GetAllLoadAvg5() []float64 {
	return monitor.LoadAvg5
}

// GetAllLoadAvg10 returns historical load_avg
func (monitor *InfoMonitor) GetAllLoadAvg10() []float64 {
	return monitor.LoadAvg10
}

// GetCurrentLoadAvgString returns current load_avg as a string
func (monitor *InfoMonitor) GetCurrentLoadAvgString() string {
	return "TODO"
}

// GetCurrentLoadAvg1String returns current load_avg as a string
func (monitor *InfoMonitor) GetCurrentLoadAvg1String() string {
	return "TODO"
}

// GetCurrentLoadAvg5String returns current load_avg as a string
func (monitor *InfoMonitor) GetCurrentLoadAvg5String() string {
	return "TODO"
}

// GetCurrentLoadAvg10String returns current load_avg as a string
func (monitor *InfoMonitor) GetCurrentLoadAvg10String() string {
	return "TODO"
}
