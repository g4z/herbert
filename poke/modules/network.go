// Package modules provides primitives for system metrics
package modules

import "encoding/json"

// NetworkMonitor structure for network metrics
type NetworkMonitor struct {
	Name    string
	RXTotal int64
	TXTotal int64
	RXRate  float64
	TXRate  float64
}

// NewNetworkMonitor returns a new NetworkMonitor structure
func NewNetworkMonitor() (*NetworkMonitor, error) {
	monitor := &NetworkMonitor{}
	err := monitor.Refresh()
	if err != nil {
		return nil, err
	}
	return monitor, nil
}

// String returns a JSON representation
func (monitor *NetworkMonitor) String() (string, error) {
	json, err := json.Marshal(monitor)
	if err != nil {
		return "", err
	}
	return string(json), nil
}

// Refresh updates the Network structure data
func (monitor *NetworkMonitor) Refresh() error {
	// TODO Implement Refresh()
	return nil
}
