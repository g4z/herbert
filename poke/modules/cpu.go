// Package modules provides primitives for system metrics
package modules

import "encoding/json"

// type CPUCore struct {

// }

// CPUMonitor structure for storing the CPU metrics
type CPUMonitor struct {
	vendorID string // eg. "GenuineIntel"
	name     string // eg. "Intel(R) Core(TM) i7-4510U CPU @ 2.00GHz"
	arch     string // eg. "x86_64"
	// family       int    // CPU Family code
	// model        int    // CPU Model code
	byteOrder string // eg. "Little Endian"
	// modes        []string
	totalCores   int   // Numer of cores
	totalTPC     int   // Number of threads per core
	totalCPS     int   // Number of cores per socket
	totalSockets int   // Number of sockets
	activeCores  []int // eg [0,1,3]
	stepping     int
	minMHZ       float64
	maxMHZ       float64
	speedMHZ     []float64
	bogoMIPS     float64
	virtType     string // eg "VT-x"
	totalNodes   int    // Number of NUMA node
	activeNodes  []int  // Active NUMA nodes
	l3CacheSize  int    // Level3 cache size
	flags        string // Space sep. list of CPU flags
	numProc      string // Number of running processes
	idleProc     string // Number of idle processes
}

// NewCPUMonitor returns a new CPUMonitor structure
func NewCPUMonitor() (*CPUMonitor, error) {
	monitor := &CPUMonitor{}
	err := monitor.Refresh()
	if err != nil {
		return nil, err
	}
	return monitor, nil
}

// String returns a JSON representation
func (monitor *CPUMonitor) String() (string, error) {
	json, err := json.Marshal(monitor)
	if err != nil {
		return "", err
	}
	return string(json), nil
}

// Refresh updates the Host struct data
func (monitor *CPUMonitor) Refresh() error {
	// monitor.RefreshMemInfo()
	return nil
}
