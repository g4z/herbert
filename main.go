package main

import (
	"fmt"
	"math"
	"time"

	"./poke"
	ui "github.com/gizak/termui"
)

var host *poke.Host

func init() {
	h, err := poke.NewHost()
	if err != nil {
		panic(err)
	}
	host = h
}

func main() {
	// dumpJSON()
	outputText()
	renderUI()
}

func dumpJSON() {
	json, _ := host.String()
	fmt.Println(json)
}

func outputText() {
	fmt.Printf("LSBDistribution: %s\n", host.Info.GetLSBDistribution())
	fmt.Printf("LSBDescription: %s\n", host.Info.GetLSBDescription())
	fmt.Printf("LSBRelease: %s\n", host.Info.GetLSBRelease())
	fmt.Printf("LSBCodename: %s\n", host.Info.GetLSBCodename())
	fmt.Printf("Hostname: %s\n", host.Info.GetHostname())
	fmt.Printf("OSType: %s\n", host.Info.GetOSType())
	fmt.Printf("OSRelease: %s\n", host.Info.GetOSRelease())
	fmt.Printf("OSVersion: %s\n", host.Info.GetOSVersion())
	fmt.Printf("Uptime: %f\n", host.Info.GetCurrentUptime())
	fmt.Printf("AllUptime: %v\n", host.Info.GetAllUptime())
	fmt.Printf("Uptime(string): %v\n", host.Info.GetCurrentUptimeString())
	fmt.Printf("Idletime: %f\n", host.Info.GetCurrentIdletime())
	fmt.Printf("AllIdletime: %v\n", host.Info.GetAllIdletime())
	fmt.Printf("Idletime(string): %v\n", host.Info.GetCurrentIdletimeString())
	fmt.Printf("LoadAvg: %v\n", host.Info.GetCurrentLoadAvg())
	fmt.Printf("LoadAvg1: %f\n", host.Info.GetCurrentLoadAvg1())
	fmt.Printf("LoadAvg5: %f\n", host.Info.GetCurrentLoadAvg5())
	fmt.Printf("LoadAvg10: %f\n", host.Info.GetCurrentLoadAvg10())
	fmt.Printf("AllLoadAvg: %v\n", host.Info.GetAllLoadAvg())
	fmt.Printf("AllLoadAvg1: %v\n", host.Info.GetAllLoadAvg1())
	fmt.Printf("AllLoadAvg5: %v\n", host.Info.GetAllLoadAvg5())
	fmt.Printf("AllLoadAvg10: %v\n", host.Info.GetAllLoadAvg10())
	fmt.Printf("LoadAvg(string): %v\n", host.Info.GetCurrentLoadAvgString())
	fmt.Printf("LoadAvg1(string): %v\n", host.Info.GetCurrentLoadAvg1String())
	fmt.Printf("LoadAvg5(string): %v\n", host.Info.GetCurrentLoadAvg5String())
	fmt.Printf("LoadAvg10(string): %v\n", host.Info.GetCurrentLoadAvg10String())

	// Memory

	fmt.Printf("MemTotal: %d\n", host.Memory.GetCurrentMemTotal())
	fmt.Printf("AllMemTotal: %v\n", host.Memory.GetAllMemTotal())
	fmt.Printf("MemFree: %d\n", host.Memory.GetCurrentMemFree())
	fmt.Printf("AllMemFree: %v\n", host.Memory.GetAllMemFree())
	fmt.Printf("MemAvailable: %d\n", host.Memory.GetCurrentMemAvailable())
	fmt.Printf("AllMemAvailable: %v\n", host.Memory.GetAllMemAvailable())
	fmt.Printf("SwapTotal: %d\n", host.Memory.GetCurrentSwapTotal())
	fmt.Printf("AllSwapTotal: %v\n", host.Memory.GetAllSwapTotal())
	fmt.Printf("SwapFree: %d\n", host.Memory.GetCurrentSwapFree())
	fmt.Printf("AllSwapFree: %v\n", host.Memory.GetAllSwapFree())
}

func renderUI() {

	if err := ui.Init(); err != nil {
		panic(err)
	}
	defer ui.Close()

	var widget interface{}
	var widgets map[string]interface{}

	widgets = make(map[string]interface{})

	widget = ui.NewPar(" +++ Crappo System Monitor +++")
	widget.(*ui.Par).Height = 1
	widget.(*ui.Par).Border = false
	widget.(*ui.Par).Bg = ui.ColorBlue
	widget.(*ui.Par).TextBgColor = ui.ColorBlue
	widget.(*ui.Par).BorderBg = ui.ColorBlue
	widgets["header"] = widget

	widget = ui.NewPar("loading...")
	widget.(*ui.Par).Height = 1
	widget.(*ui.Par).Border = false
	widget.(*ui.Par).Bg = ui.ColorBlue
	widget.(*ui.Par).TextBgColor = ui.ColorBlue
	widget.(*ui.Par).BorderBg = ui.ColorBlue
	widgets["clock"] = widget

	widget = ui.NewPar("")
	widget.(*ui.Par).BorderLabel = "[ System Info ]"
	widget.(*ui.Par).BorderLabelFg = ui.ColorWhite
	widget.(*ui.Par).Height = 6
	widgets["sys_info"] = widget

	widget = ui.NewPar("")
	widget.(*ui.Par).BorderLabel = "[ LSB Data ]"
	widget.(*ui.Par).BorderLabelFg = ui.ColorWhite
	widget.(*ui.Par).Height = 6
	widgets["lsb_info"] = widget

	// Memory Usage Gauge

	widget = ui.NewGauge()
	widget.(*ui.Gauge).Width = 50
	widget.(*ui.Gauge).Height = 3
	widget.(*ui.Gauge).BorderLabel = "[ Memory Usage ]"
	widget.(*ui.Gauge).BorderLabelFg = ui.ColorWhite
	widget.(*ui.Gauge).BarColor = ui.ColorGreen
	// widget.(*ui.Gauge).BorderFg = ui.ColorWhite
	// widget.(*ui.Gauge).BorderLabelFg = ui.ColorCyan
	// widget.(*ui.Gauge).LabelAlign = ui.AlignLeft
	widgets["memory_gauge"] = widget

	// Swap Usage Gauge

	widget = ui.NewGauge()
	widget.(*ui.Gauge).Width = 50
	widget.(*ui.Gauge).Height = 3
	widget.(*ui.Gauge).BorderLabel = "[ Swap Usage ]"
	widget.(*ui.Gauge).BorderLabelFg = ui.ColorWhite
	widget.(*ui.Gauge).BarColor = ui.ColorGreen
	// widget.(*ui.Gauge).BorderFg = ui.ColorWhite
	widgets["swap_gauge"] = widget

	// Load Average Sparklines

	sparkline1 := ui.NewSparkline()
	sparkline1.Height = 1
	sparkline1.Title = "1min"
	sparkline1.LineColor = ui.ColorGreen
	sparkline1.TitleColor = ui.ColorGreen

	sparkline2 := ui.NewSparkline()
	sparkline2.Height = 1
	sparkline2.Title = "5min"
	sparkline2.LineColor = ui.ColorYellow
	sparkline2.TitleColor = ui.ColorYellow

	sparkline3 := ui.NewSparkline()
	sparkline3.Height = 1
	sparkline3.Title = "10min"
	sparkline3.LineColor = ui.ColorCyan
	sparkline3.TitleColor = ui.ColorCyan

	widget = ui.NewSparklines(sparkline1, sparkline2, sparkline3)
	widget.(*ui.Sparklines).BorderLabel = "[ Load Average ]"
	widget.(*ui.Sparklines).BorderLabelFg = ui.ColorWhite
	widget.(*ui.Sparklines).Height = 8
	widgets["loadavg_sparkline"] = widget

	ui.Body.AddRows(
		ui.NewRow(
			ui.NewCol(10, 0, widgets["header"].(*ui.Par)),
			ui.NewCol(2, 0, widgets["clock"].(*ui.Par)),
		),
		ui.NewRow(
			ui.NewCol(5, 0, widgets["sys_info"].(*ui.Par)),
			ui.NewCol(3, 0, widgets["lsb_info"].(*ui.Par)),
		),
		ui.NewRow(
			ui.NewCol(5, 0, widgets["memory_gauge"].(*ui.Gauge)),
		),
		ui.NewRow(
			ui.NewCol(5, 0, widgets["swap_gauge"].(*ui.Gauge)),
		),
		ui.NewRow(
			ui.NewCol(5, 0, widgets["loadavg_sparkline"].(*ui.Sparklines)),
		),
	)

	ui.Body.Align()

	ui.Render(ui.Body)

	ui.Handle("q", func(ui.Event) {
		ui.StopLoop()
	})

	ui.Handle("<Resize>", func(e ui.Event) {
		payload := e.Payload.(ui.Resize)
		ui.Body.Width = payload.Width
		ui.Body.Align()
		ui.Clear()
		ui.Render(ui.Body)
	})

	ticker := time.NewTicker(time.Second)
	counter := 0
	go func() {
		for {

			// update all data

			if err := host.Refresh(); err != nil {
				panic(err) // crash
			}

			// update clock

			widgets["clock"].(*ui.Par).Text =
				fmt.Sprintf("[ %s ]", time.Now().Format("Jan 1 15:04"))

			// update lsb_info

			widgets["lsb_info"].(*ui.Par).Text =
				fmt.Sprintf(
					"Dist: %s\nDesc: %s\nRel: %s\nCode: %s",
					host.Info.GetLSBDistribution(),
					host.Info.GetLSBDescription(),
					host.Info.GetLSBRelease(),
					host.Info.GetLSBCodename(),
				)

			// update sys_info

			widgets["sys_info"].(*ui.Par).Text =
				fmt.Sprintf(
					"Hostname: %s\nUptime: %s\nKernel: %s %s\nVersion: %s",
					host.Info.GetHostname(),
					host.Info.GetCurrentUptimeString(),
					host.Info.GetOSType(),
					host.Info.GetOSRelease(),
					host.Info.GetOSVersion(),
				)

			// update memory and swap gauges

			// widget.(*ui.Gauge).Label = "{{percent}}% (100MBs free)"
			// free := host.Memory.GetCurrentMemAvailable() - host.Memory.GetCurrentMemAvailable()
			// widgets["memory_gauge"].(*ui.Gauge).Label = "{{percent}}% (100MBs free)"
			widgets["memory_gauge"].(*ui.Gauge).Percent =
				int(math.Round(100 - (float64(host.Memory.GetCurrentMemAvailable())/float64(host.Memory.GetCurrentMemTotal()))*100.0))

			widgets["swap_gauge"].(*ui.Gauge).Percent =
				int(math.Round(100 - (float64(host.Memory.GetCurrentSwapFree())/float64(host.Memory.GetCurrentSwapTotal()))*100.0))

			// update load_avg sparklines

			var data []int

			for _, metric := range host.Info.GetAllLoadAvg1() {
				data = append(data, int((metric * 4)))
			}
			widgets["loadavg_sparkline"].(*ui.Sparklines).Lines[0].Data = data

			for _, metric := range host.Info.GetAllLoadAvg5() {
				data = append(data, int((metric * 4)))
			}
			widgets["loadavg_sparkline"].(*ui.Sparklines).Lines[1].Data = data

			for _, metric := range host.Info.GetAllLoadAvg10() {
				data = append(data, int((metric * 4)))
			}
			widgets["loadavg_sparkline"].(*ui.Sparklines).Lines[2].Data = data

			// ui.Clear()
			ui.Render(ui.Body)

			<-ticker.C
			counter++
		}
	}()

	ui.Loop()

}
